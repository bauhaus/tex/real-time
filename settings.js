module.exports = {
  internal: {
    realTime: {
      host: process.env.LISTEN_ADDRESS || "0.0.0.0",
    },
  },
};
